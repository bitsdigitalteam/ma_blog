
/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */
jQuery.noConflict();
jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};

//
var page_loaded = false;
// animations
var elementsToAnimate = [];
//
var headerHeight = ''

jQuery(document).ready(function() {	
	
	jQuery('.search_form input').keyup(function(){
		var search_val = jQuery(this).val().toLowerCase();
		
		jQuery('.itemFullText section').each(function(){
			var this_q = jQuery(this).find('.faq').html();
			if(this_q!=undefined){
				this_q = this_q.toLowerCase();
				if( this_q.indexOf( search_val )==-1 && search_val.lenght!=0 ){
					jQuery(this).hide();
				}else{
					jQuery(this).show();
				}
			}
			
		});
		
	});
	
	jQuery('.faq_block').hide();
	jQuery('.faq').each(function(){
		jQuery(this).click(function(){
			if( jQuery(this).siblings(".faq_block").is(":visible") ){
				jQuery(this).siblings(".faq_block").slideUp();
			}else{
				jQuery(this).siblings(".faq_block").slideDown();
			}
		})
	});
	
	
	var mod_lang_height = jQuery('.mod-languages').height();
	jQuery('.lang-active').html( jQuery('.lang-active a').html() );
	jQuery('.mod-languages').click(function(){
		if( jQuery(this).attr('status')!='open'){
			jQuery(this).height('auto');
			jQuery(this).attr('status','open');
		}else{
			jQuery(this).height(mod_lang_height);
			jQuery(this).attr('status','');
		}
	});
	
	jQuery('.itemBackToTop').click(function(){
		$('html, body').animate({scrollTop: 0}, 1500);
	});
	
	//facebook pop-up
	if ( getCookie('hideFb') != '1' &&  (( (location.pathname).indexOf('/statya/')!=-1 && (location.pathname).indexOf('/statya/tag/')!=3 ) || (location.pathname).indexOf('/stattya/')!=-1 && (location.pathname).indexOf('/stattya/tag/')!=3) ) {
		var delay_popup = 15000;
		setTimeout("document.getElementById('fb-overlay').style.display='block'", delay_popup);
		///close facebook pop-up
		jQuery('.close-fb, .end-box').on('click', function(e) {
			e.preventDefault();
			jQuery('#fb-overlay').hide();
		});	

		setCookie('hideFb', '1', 7);
	};
	
	
	var active_li = jQuery('.mod-languages .lang-block').find('li').sort(sort_li);
	jQuery('.mod-languages .lang-block').append(active_li);
	function sort_li(a, b) {
        return a.className < b.className;
	}
	
	
	
	//
	page_loaded = true;
	
	headerHeight = jQuery('#gkHeader').outerHeight();
	// smooth anchor scrolling
	//new SmoothScroll(); 
	// style area
	if(jQuery('#gkStyleArea')){
		jQuery('#gkStyleArea').find('a').each(function(i,element){
			jQuery(element).click(function(e){
	            e.preventDefault();
	            e.stopPropagation();
				changeStyle(i+1);
			});
		});
	}
	// font-size switcher
	if(jQuery('#gkTools') && jQuery('#gkMainbody')) {
		var current_fs = 100;
		
		jQuery('#gkMainbody').css('font-size', current_fs+"%");
		
		jQuery('#gkToolsInc').click(function(e){ 
			e.stopPropagation();
			e.preventDefault(); 
			if(current_fs < 150) {  
				jQuery('#gkMainbody').animate({ 'font-size': (current_fs + 10) + "%"}, 200); 
				current_fs += 10; 
			} 
		});
		jQuery('#gkToolsReset').click(function(e){ 
			e.stopPropagation();
			e.preventDefault(); 
			jQuery('#gkMainbody').animate({ 'font-size' : "100%"}, 200); 
			current_fs = 100; 
		});
		jQuery('#gkToolsDec').click(function(e){ 
			e.stopPropagation();
			e.preventDefault(); 
			if(current_fs > 70) { 
				jQuery('#gkMainbody').animate({ 'font-size': (current_fs - 10) + "%"}, 200); 
				current_fs -= 10; 
			} 
		});
	}
	// K2 font-size switcher fix
	if(jQuery('#fontIncrease') && jQuery('.itemIntroText')) {
		jQuery('#fontIncrease').click(function() {
			jQuery('.itemIntroText').attr('class', 'itemIntroText largerFontSize');
		});
		
		jQuery('#fontDecrease').click( function() {
			jQuery('.itemIntroText').attr('class', 'itemIntroText smallerFontSize');
		});
	}
	
	if(jQuery('#system-message-container a.close')){
		  jQuery('#system-message-container').find('a.close').each(function(i, element){
		  		jQuery('#system-message-container').css({'display' : 'block'});	
	           jQuery(element).click(function(e){
	           		e.preventDefault();
	           		e.stopPropagation();
	                jQuery(element).parents().eq(2).fadeOut();
	                (function() {
	                     jQuery(element).parents().eq(2).css({'display': 'none'});
	                }).delay(500);
	           });
	      });
	 } 
	
	// create the list of elements to animate
	jQuery('.gkHorizontalSlideRightColumn').each(function(i,element) {
		elementsToAnimate.push(['animation', element, jQuery(element).offset().top]);
	});
	
	jQuery('.layered').each(function(i,element) {
		elementsToAnimate.push(['animation', element, jQuery(element).offset().top]);
	});
	
	jQuery('.gkPriceTableAnimated').each(function(i,element) {
		elementsToAnimate.push(['queue_anim', element, jQuery(element).offset().top]);
	});
});

//
jQuery(window).scroll(function() {
	// menu animation
	
	
	if(page_loaded && jQuery('body').hasClass('imageBg')) {
		// if menu is not displayed now
		if(jQuery(window).scrollTop() > headerHeight && !jQuery('#gkMenuWrap').hasClass('active')) {
			//document.id('gkHeaderNav').inject(document.id('gkMenuWrap'), 'inside');
			jQuery('#gkMenuWrap').append(jQuery('#gkHeaderNav'));
			jQuery('#gkHeader').attr('class', 'gkNoMenu');
			// hide
			jQuery('#gkMenuWrap').attr('class', 'active');
		}
		//
		if(jQuery(window).scrollTop() <= headerHeight && jQuery('#gkMenuWrap').hasClass('active')) {
			jQuery('#gkHeader').first('div').css('display', 'block');
			jQuery('#gkHeader').first('div').prepend(jQuery('#gkHeaderNav'));
			jQuery('#gkHeader').attr('class', '');
			jQuery('#gkMenuWrap').attr('class', '');
		}
	}
	// animate all right sliders
	if(elementsToAnimate.length > 0) {		
		// get the necessary values and positions
		var currentPosition = jQuery(window).scrollTop();
		var windowHeight = jQuery(window).outerHeight();
		
		// iterate throught the elements to animate
		if(elementsToAnimate.length) {
			for(var i = 0; i < elementsToAnimate.length; i++) {
				if(elementsToAnimate[i][2] < currentPosition + (windowHeight / 1.2)) {
					// create a handle to the element
					var element = elementsToAnimate[i][1];
					// check the animation type
					if(elementsToAnimate[i][0] == 'animation') {
						//console.log('(XXX)' + elementsToAnimate[i][2]);
						gkAddClass(element, 'loaded', false);
						// clean the array element
						elementsToAnimate[i] = null;
					}
					// if there is a queue animation
					else if(elementsToAnimate[i][0] == 'queue_anim') {
						//console.log('(XXX)' + elementsToAnimate[i][2]);
						jQuery(element).find('dl').each(function(j, item) {
							gkAddClass(item, 'loaded', j);
						});
						// clean the array element
						elementsToAnimate[i] = null;
					}
				}
			}
			// clean the array
			elementsToAnimate = elementsToAnimate.clean();
		}
	}
});


function getCookie(c_name) {
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	if (c_start == -1) {
		c_start = c_value.indexOf(c_name + "=");
	}
	if (c_start == -1) {
		c_value = null;
	} else {
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1) {
			c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start, c_end));
	}
	return c_value;

}

function setCookie(c_name, value, exdays) {
	var exdate = new Date();
	exdays = exdays*1;
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString() + "; path=/;");
	document.cookie = c_name + "=" + c_value;
}


//
function gkAddClass(element, cssclass, i) {
	var delay = jQuery(element).attr('data-delay');
	
	if(!delay) {
		delay = (i !== false) ? i * 150 : 0;
	}

	setTimeout(function() {
		element.addClass(cssclass);
	}, delay);
}
//

jQuery(window).ready(function() {
	//
	var menuwrap = new jQuery('<div />', {
		'id': 'gkMenuWrap'
	});
	
	//
	jQuery('body').append(menuwrap);
	//
	if(!jQuery('body').hasClass('imageBg')) {
		jQuery('#gkMenuWrap').append(jQuery('#gkHeaderNav'));
		jQuery('#gkHeader').attr('class', 'gkNoMenu');
		jQuery('#gkHeader > div').first().css('display', 'none');
		jQuery('#gkMenuWrap').attr('class', 'active');
	}
	//
	// some touch devices hacks
	//
	
	// hack modal boxes ;)
	jQuery('a.modal').each(function(i,link) {
		// register start event
		var lasttouch = [];
		// here
		jQuery(link).bind('touchstart', function(e) {
			lasttouch = [link, new Date().getTime()];
		});
		// and then
		jQuery(link).bind('touchend', function(e) {
			// compare if the touch was short ;)
			if(lasttouch[0] == link && Math.abs(lasttouch[1] - new Date().getTime()) < 500) {
				window.location = jQuery(link).attr('href');
			}
		});
	});
	
});

// Function to change styles
function changeStyle(style){
	var file1 = $GK_TMPL_URL+'/css/style'+style+'.css';
	var file2 = $GK_TMPL_URL+'/css/typography/typography.style'+style+'.css';
	var file3 = $GK_TMPL_URL+'/css/typography/typography.iconset.style'+style+'.css';
	jQuery('head').append('<link rel="stylesheet" href="'+file1+'" type="text/css" />');
	jQuery('head').append('<link rel="stylesheet" href="'+file2+'" type="text/css" />');
	jQuery('head').append('<link rel="stylesheet" href="'+file3+'" type="text/css" />');
	jQuery.cookie('gk_simplicity_j30_style', style, { expires: 365, path: '/' });
}
