<?php

/**
 * @package		K2
 * @author		GavickPro http://gavick.com
 */

// no direct access
defined('_JEXEC') or die;

?>

<script>
	jQuery(document).ready(function(){
		var str = jQuery('.header_text').html();
		str = str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
		jQuery('.header_text').html(str);
	});
</script>


<section id="k2Container" class="genericView<?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">
	<?php /* if($this->params->get('show_page_title')): */?>
	<header>
		<h2 class="header_text"><?php echo $this->escape($this->params->get('page_title')); ?></h2>
	</header>
	<?php /*endif; */?>
	
	<?php if(count($this->items)): ?>
	<section class="itemList">
		<?php 
		$gkItemCounter = 0;
		foreach($this->items as $item): 
		$gkItemCounter++;
		?>
		<article class="itemView" id="<?php 
				if( $gkItemCounter==1 ){echo 'first_tags';}
				else 
					if( $gkItemCounter==2 || $gkItemCounter==3 ){echo 'second_tags';}else{echo 'next_tags';}
			?>">
			
			<!--
			<?php if($item->params->get('genericItemImage') && !empty($item->imageGeneric)): ?>
			<div class="itemImageBlock"> <a class="itemImage" href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo $item->image_caption; else echo $item->title; ?>"> <img src="<?php echo $item->imageGeneric; ?>" alt="<?php if(!empty($item->image_caption)) echo $item->image_caption; else echo $item->title; ?>" style="width:<?php echo $item->params->get('itemImageGeneric'); ?>px; height:auto;" /> </a> </div>
			<?php endif; ?>
			
			<header>
					<?php if($item->params->get('tagItemCategory')): ?>
					<ul>
						<?php if($item->params->get('tagItemCategory')) : ?>
						<li class="itemCategory"> <span><?php echo JText::_('K2_PUBLISHED_IN'); ?></span> <a href="<?php echo $item->category->link; ?>"><?php echo $item->category->name; ?></a> </li>
						<?php endif; ?>
						
						<?php if($item->params->get('tagItemDateCreated')): ?>
						<li class="itemDate">
							<time datetime="<?php echo JHtml::_('date', $item->created, JText::_(DATE_W3C)); ?>">
								<?php echo JHTML::_('date', $item->created , JText::_('d M Y')); ?>
							</time>
						</li>
						<?php endif; ?>
					</ul>
					<?php endif; ?>
					
					<?php if($item->params->get('tagItemTitle')): ?>
					<h2>
						<?php if ($item->params->get('tagItemTitleLinked')): ?>
						<a href="<?php echo $item->link; ?>"> <?php echo $item->title; ?> </a>
						<?php else: ?>
						<?php echo $item->title; ?>
						<?php endif; ?>
					</h2>
					<?php endif; ?>
			</header>
			
			<div class="itemBody">
				<?php if($item->params->get('tagItemIntroText')): ?>
				<div class="itemIntroText"> <?php echo $item->introtext; ?> </div>
				<?php endif; ?>
				
				<?php if($item->params->get('tagItemExtraFields') && count($item->extra_fields)): ?>
				<div class="itemExtraFields">
						<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
						<ul>
							<?php foreach ($item->extra_fields as $key=>$extraField): ?>
							<?php if($extraField->value != ''): ?>
							<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
								<?php if($extraField->type == 'header'): ?>
								<h4 class="tagItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
								<?php else: ?>
								<span class="tagItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
								<span class="tagItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
								<?php endif; ?>		
							</li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
				</div>
				<?php endif; ?>
				
				<?php if ($item->params->get('tagItemReadMore')): ?>
				<a class="itemReadMore button" href="<?php echo $item->link; ?>"> <?php echo JText::_('K2_READ_MORE'); ?> </a>
				<?php endif; ?>
			</div>
			
			-->
			
			<?php if($gkItemCounter==1) {?>
					<div class="post_title">
					<?php if($item->params->get('catItemTags') && count($item->tags)): ?>
						<li class="itemTagsBlock">
							<?php foreach ($item->tags as $tag): ?>
							<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?> </a>
							<?php endforeach; ?>
						</li>
					<?php endif; ?>
					
					
					<?php if($item->params->get('catItemTitle')): ?>
					<h2>
						<?php if ($item->params->get('catItemTitleLinked')): ?>
							<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
						<?php else: ?>
							<?php echo $item->title; ?>
						<?php endif; ?>	
					</h2>
					<?php endif; ?>
					</div>
					
					<a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" class="itemImage">
						<img src="<?php echo $item->imageXLarge; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" />
					</a>
			
			<?php } ?>
			
			
			
			<?php if($gkItemCounter >=2 ) { ?>
				<a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" class="itemImage">
					<img src="<?php echo $item->imageMedium; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" />
				</a>
				
				<div class="post_title">
					<?php if($item->params->get('catItemTags') && count($item->tags)): ?>
						<li class="itemTagsBlock">
							<?php foreach ($item->tags as $tag): ?>
							<a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?> </a>
							<?php endforeach; ?>
						</li>
					<?php endif; ?>
					
					
					<?php if($item->params->get('catItemTitle')): ?>
					<h2>
						<?php if ($item->params->get('catItemTitleLinked')): ?>
							<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
						<?php else: ?>
							<?php echo $item->title; ?>
						<?php endif; ?>	
					</h2>
					<?php endif; ?>
				</div>
				
				
				<?php if($item->params->get('catItemIntroText')): ?>
					<div class="itemIntroText"> <?php echo strip_tags($item->introtext); ?> </div>
				<?php endif; ?>
			
			<?php } ?>

			
		</article>
		<?php endforeach; ?>
	</section>
	
	<?php if($this->params->get('tagFeedIcon',1)): ?>
	<a class="k2FeedIcon" href="<?php echo $this->feed; ?>"><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></a>
	<?php endif; ?>
	
	<?php if($this->pagination->getPagesLinks()): ?>
	<?php echo str_replace('</ul>', '<li class="counter">'.$this->pagination->getPagesCounter().'</li></ul>', $this->pagination->getPagesLinks()); ?>
	<?php endif; ?>
	<?php endif; ?>
</section>
