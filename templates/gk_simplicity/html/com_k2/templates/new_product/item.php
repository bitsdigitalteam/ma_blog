<?php

/**
 * @package		K2
 * @author		GavickPro http://gavick.com
 */

// no direct access
defined('_JEXEC') or die;

// Template override
jimport('joomla.filesystem.file');
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
$templateParams = JFactory::getApplication()->getTemplate(true)->params;
$override = JPATH_SITE . DS . 'templates' . DS . 'gk_overrides' . DS . $templateParams->get('custom_override', '-1') . DS . 'html' . DS;
$override .=  'com_k2' . DS . 'templates' . DS . 'default' . DS . 'item.php';

$replace_arr = array('а','о','и','е','ё','э','ы','у','ю','я','і','ї','є');

if(
	$templateParams->get('custom_override', '-1') !== '-1' && 
	JFile::exists($override) &&
	__FILE__ !== $override
) :
	include_once($override);
else :
?>

<?php
// Code used to generate the page elements
$params = $this->item->params;
$k2ContainerClasses = (($this->item->featured) ? ' itemIsFeatured' : '') . ($params->get('pageclass_sfx')) ? ' '.$params->get('pageclass_sfx') : ''; 

$cur_url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$cur_url = preg_replace('@%[0-9A-Fa-f]{1,2}@mi', '', htmlspecialchars($cur_url, ENT_QUOTES, 'UTF-8'));

$document = JFactory::getDocument();
$templateSettings = JFactory::getApplication()->getTemplate(true)->params;
$renderer = $document->loadRenderer('modules');

?>
<?php if(JRequest::getInt('print')==1): ?>

<a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print(); return false;"> <?php echo JText::_('K2_PRINT_THIS_PAGE'); ?> </a>
<?php endif; ?>
<article id="k2Container" class="itemView single-page <?php echo $k2ContainerClasses; ?> single_product"> 
	
<style type="text/css">
	.gkPage.custom-p,.custom-p #gkContentWrap,#gkPageContent {
    max-width: 1920px;
    margin: 0px!important;
    box-shadow: none; 
    padding: 0!important;
}
.custom_navigator{
	display: none;
}
.raxo_products{
	padding: 0;
}
#gkMainbodyBottom{
	padding-top: 0!important;
}
</style>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('.allmode-topitem h3 a').each(function(){			
			if(jQuery(this).html()== jQuery('h1').html()){
				jQuery(this).closest('.allmode-topitem').remove();
			}
		});
		
		
		jQuery('.street_search').keyup(function(){
			var search_val = jQuery(this).val().toLowerCase();
			jQuery('.adresses li').each(function(){
				if( jQuery(this).attr('street').toLowerCase().indexOf(search_val)==-1 && search_val.length!=0 ){
					jQuery(this).hide();
				}else{
					jQuery(this).show();
				}
			});
			
		});
		
	});
</script>
	
		
		
		
		
		<?php 
			//echo '<pre>';print_r($this->item); echo '</pre>';
			
			//$this->item->title
			//$this->item->alias
			
			//$this->item->alias
			
			//$this->item->extra_fields[0]->value //img_original_png
			
			//$this->item->extra_fields[1]->value //img_1
			//$this->item->extra_fields[2]->value //text_1
			
			//$this->item->extra_fields[3]->value //img_2
			//$this->item->extra_fields[4]->value //text_2
			
			//$this->item->extra_fields[5]->value //img_3
			//$this->item->extra_fields[6]->value //text_3
			
			//$this->item->extra_fields[7]->value //img_4
			//$this->item->extra_fields[8]->value //text_4
			
			//$this->item->extra_fields[9]->value //img_5
			//$this->item->extra_fields[10]->value //text_5
			
			//$this->item->extra_fields[11]->value //img_6
			//$this->item->extra_fields[12]->value //text_6
			
			//$this->item->extra_fields[13]->value //img_7
			//$this->item->extra_fields[14]->value //text_7
			
			//$this->item->extra_fields[15]->value //img_8
			//$this->item->extra_fields[16]->value //text_8
			
			//$this->item->extra_fields[17]->value //csv import
			
			preg_match_all('#src="(.*?)"#is',$this->item->extra_fields[18]->value,$marker);
			$marker = $marker[1][0];
			$marker_claster = $this->item->extra_fields[20]->value;
			
		?>
		
		<div class="new_product <?php echo $this->item->extra_fields[19]->value; ?> after_readmore">
			<div class="full_new_product">
				<div class="new_product_title">
					<h1><?php echo $this->item->title; ?></h1>
				</div>
				
				<div class="new_product_desc">
					<div class="new_product_image">
						<div class="product_image">
							<?php echo $this->item->extra_fields[0]->value;?>
						</div>
					</div>
					
					<?php echo $this->item->fulltext; ?>
		
			
				</div>
				
				<div class="new_product_text product_map">
					<div id="new_product_map">&nbsp;</div>
					<div class="search_city">
						<label for="exact_city"><?php echo JText::_('K2_CITY'); ?></label>
						<select>
							<option></option>
							<?php
								$count = 0;
								$city = explode("\n",strip_tags($this->item->extra_fields[17]->value));
								$rows='';
								$city_arr = array();
								for($i=0; $i<count($city); $i++){
									$temp = explode(';',trim($city[$i]));
									$count++;
									$rows .='<li class="'.$temp[2].'" street="'.$temp[0].', '.$temp[1].'">'.$temp[0].'</li>';
									if(!in_array($temp[1],$city_arr)){
										echo '<option value="'.$temp[1].'" >'.$temp[1].'</option>';
										$city_arr[] = $temp[1];
									}
								}
							?>
						</select>
					</div>
					<div class="street">
						<label for="exact_city"><?php echo JText::_('K2_STREET'); ?></label>
						<input type="text" class="street_search">
					</div>
					<div class="adresses">
						<ul>
							<?php echo $rows; ?>
						</ul>
						<div class="all_groсeries"><?php echo JText::_('K2_TOTAL_STORES'); ?> <span class="groceries_number"><?php echo $count ;?></span></div>
					</div>
				</div>


				<div class="icons_products">
					<div class="icons_row">
						<?php if($this->item->extra_fields[1]->value!='' && $this->item->extra_fields[2]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[1]->value;?>
							<p class="ench"><?php echo $this->item->extra_fields[2]->value ?></p>
						</div>
						<?php 
							}
							if($this->item->extra_fields[3]->value!='' && $this->item->extra_fields[4]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[3]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[4]->value ?></p>
						</div>
						<?php }?>
					</div>
					<div class="icons_row">
						<?php if($this->item->extra_fields[5]->value!='' && $this->item->extra_fields[6]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[5]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[6]->value ?></p>
						</div>
						<?php 
							}
							if($this->item->extra_fields[7]->value!='' && $this->item->extra_fields[8]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[7]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[8]->value ?></p>
						</div>
							<?php }?>
					</div>	

					<div class="icons_row">
						<?php if($this->item->extra_fields[9]->value!='' && $this->item->extra_fields[10]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[9]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[10]->value ?></p>
						</div>
						<?php 
							}
							if($this->item->extra_fields[11]->value!='' && $this->item->extra_fields[12]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[11]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[12]->value ?></p>
						</div>
						<?php }?>
					</div>
					<div class="icons_row">
						<?php if($this->item->extra_fields[13]->value!='' && $this->item->extra_fields[14]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[13]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[14]->value ?></p>
						</div>
						<?php 
							}
							if($this->item->extra_fields[15]->value!='' && $this->item->extra_fields[16]->value!='' ) {?>
						<div class="product_ench">
							<?php echo $this->item->extra_fields[15]->value ?>
							<p class="ench"><?php echo $this->item->extra_fields[16]->value ?></p>
						</div>
							<?php }?>
					</div>
				</div>

			</div>
		</div>
		
		
		
		

<!-- Social Buttons -->
<?php
$url = & JFactory::getURI();
$fb = file_get_contents("http://graph.facebook.com/?id=".$url);
$fb = json_decode($fb);
$fb = (int)$fb->shares;

#$vk = file_get_contents("https://vk.com/share.php?act=count&index=1&url=".$url);
#preg_match("/, (.*?)\\);/", $vk, $vkp);
#$vk = (int)$vkp[1];


#$ok = file_get_contents("https://connect.ok.ru/dk?st.cmd=extLike&uid=odklcnt0&ref=".$url);
#preg_match( '/^ODKL\.updateCount\(\'odklcnt0\',\'(\d+)\'\);$/i', $ok, $okp);
#$ok = (int)$okp[1];
?>
<div class="share-buttons">
	<div class="col-xs-6 col-sm-6">
		<a class="share-fb2 disabled" href="https://www.facebook.com/sharer.php?u=<?php echo $url; ?>"  
			onclick="popupWin = window.open(this.href,'contacts','location,width=490,height=368,top=0'); popupWin.focus(); return false" target="_blank">
			<button><i class="fa fa-facebook"></i><i class="counter"><?php echo $fb; ?></i></button>
		</a>
	</div>
	<!--
	<div class="col-xs-6 col-sm-3">
		<a class="share-vk2" href="https://vk.com/share.php?url=<?php echo $url; ?>"  
			onclick="popupWin = window.open(this.href,'contacts','location,width=490,height=368,top=0'); popupWin.focus(); return false" target="_blank">
			<button><i class="fa fa-vk"></i><i class="counter"><?php echo $vk; ?></i></button>
		</a>
	</div>
	<div class="col-xs-6 col-sm-3">
		<a class="share-od2" href="https://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1&st._surl=<?php echo $url; ?>" 
			onclick="popupWin = window.open(this.href,'contacts','location,width=490,height=368,top=0'); popupWin.focus(); return false" target="_blank">
			<button><i class="fa fa-odnoklassniki"></i><i class="counter"><?php echo $ok; ?></i></button>
		</a>
	</div>
	-->	
	<div class="col-xs-6 col-sm-6">
		<a class="share-twitter disabled" href="https://twitter.com/share?url=<?php $url; ?>"  
			onclick="popupWin = window.open(this.href,'contacts','location,width=490,height=368,top=0'); popupWin.focus(); return false" target="_blank">
			<button><i class="fa fa-twitter"></i><i class="counter"></i></button>
		</a>
	</div>
</div>


		          </div>

<!-- /Social Buttons -->
				  
		     </div>
		 </div>
		 
		 <?php 
			/*
		  	if($document->countModules('sidebar')) {
		  		echo '<aside id="gkSidebar">';
		  		echo '<div>';
		  		echo $renderer->render('sidebar', array('style' => 'gk_style'), null); 
		  		echo '</div>';
		  		echo '</aside>';
		  	}
			*/
		 ?>
	</div>
</article>
<?php endif; 
	
	
	$query = 'SELECT `wz6d7_k2_categories`.`alias` as `cat_alias`, `wz6d7_k2_items`.`id`,`wz6d7_k2_items`.`alias`,`wz6d7_k2_items`.`extra_fields`,`wz6d7_k2_items`.`title`,`wz6d7_k2_items`.`catid`,`wz6d7_k2_items`.`introtext` FROM `wz6d7_k2_items` LEFT JOIN `wz6d7_k2_tags_xref` ON `wz6d7_k2_tags_xref`.`itemID` = `wz6d7_k2_items`.`id` LEFT JOIN `wz6d7_k2_tags` ON `wz6d7_k2_tags`.`id` = `wz6d7_k2_tags_xref`.`tagID` LEFT JOIN `wz6d7_k2_categories` ON `wz6d7_k2_categories`.`id`=`wz6d7_k2_items`.`catid` WHERE `wz6d7_k2_items`.`catid`='.$this->item->catid.' AND `wz6d7_k2_items`.`id`<>'.$this->item->id.' AND `wz6d7_k2_items`.`published`=1 ORDER BY `wz6d7_k2_items`.`created` DESC';
	
	$db =& JFactory::getDBO();
	$db->setQuery($query);
	$obj = $db->loadObjectList();
	
	foreach($obj as $ob){
	$ob->extra_fields = json_decode($ob->extra_fields);
	
	$link = urldecode(JRoute::_(K2HelperRoute::getItemRoute($ob->id.':'.urlencode($ob->alias), $ob->catid.':'.urlencode($ob->cat_alias))));
	
?>

		

		<div class="new_product <?php echo $ob->extra_fields[19]->value;?> before_readmore">
			<div>
				<div class="new_product_image">
					<a href="<?php echo $link; ?>">
						<div class="product_image">
							<img src="/blog/<?php echo $ob->extra_fields[0]->value;?>">
						</div>
					</a>
					
					<a href="<?php echo $link; ?>">
						<div class="product_readmore">
							<?php echo JText::_(K2_BUY); ?>
						</div>
					</a>
					
				</div>
			
				<div class="new_product_text">
					<a href="<?php echo $link; ?>">
						<div class="new_product_title">
							<h1><?php echo $ob->title; ?></h1>
						</div>
					</a>
					
					<div class="new_product_desc">
						
						<?php echo $ob->introtext; ?>
						
						<div class="icons_row">
							<?php if($ob->extra_fields[1]->value!='' && $ob->extra_fields[2]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog<?php echo $ob->extra_fields[1]->value;?>">
								<p class="ench"><?php echo $ob->extra_fields[2]->value ?></p>
							</div>
							<?php 
								}
								if($ob->extra_fields[3]->value!='' && $ob->extra_fields[4]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[3]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[4]->value ?></p>
							</div>
							<?php }?>
						</div>
						<div class="icons_row">
							<?php if($ob->extra_fields[5]->value!='' && $ob->extra_fields[6]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[5]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[6]->value ?></p>
							</div>
							<?php 
								}
								if($ob->extra_fields[7]->value!='' && $ob->extra_fields[8]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[7]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[8]->value ?></p>
							</div>
								<?php }?>
						</div>
						
						<div class="icons_row">
							<?php if($ob->extra_fields[9]->value!='' && $ob->extra_fields[10]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[9]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[10]->value ?></p>
							</div>
							<?php 
								}
								if($ob->extra_fields[11]->value!='' && $ob->extra_fields[12]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[11]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[12]->value ?></p>
							</div>
							<?php }?>
						</div>
						<div class="icons_row">
							<?php if($ob->extra_fields[13]->value!='' && $ob->extra_fields[14]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[13]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[14]->value ?></p>
							</div>
							<?php 
								}
								if($ob->extra_fields[15]->value!='' && $ob->extra_fields[16]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $ob->extra_fields[15]->value ?>">
								<p class="ench"><?php echo $ob->extra_fields[16]->value ?></p>
							</div>
								<?php }?>
						</div>
						
					</div>
				</div>
			</div>
		</div>




<?php 
	}
	
	$coor = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=Киев,%20улица%20Крещатик%201&key=AIzaSyCkgVHi1bRyvj5Z7ghkAktChEL36JtidLQ&language=ru"));
	
?>

<script>
	
	function initMap() {
		
		var center = new google.maps.LatLng(48.4888367, 31.6385228);
		var infoWindow = new google.maps.InfoWindow(), marker, i;
        var map = new google.maps.Map(document.getElementById('new_product_map'), {
          zoom: 6,
          center: center,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        
        var markers = [];
		var infoWindowContent = [];
		
		<?php 
		
			$address = array();
			$query = "SELECT * FROM `coor` WHERE 1";
			$db->setQuery($query);
			$obj = $db->loadObjectList();
			foreach($obj as $ob){
				$address[$ob->address] = $ob->lat.', '.$ob->lng;
			}
			
			for($i=0; $i<count($city); $i++){
				if($city[$i]==''){ continue; }
				
				$temp = explode(';',$city[$i]);
			
				$latLng = $address[$temp[0].','.$temp[1]];

			if($latLng == ''){
				$coor = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=".str_replace(' ','%20',$temp[0].','.$temp[1])."&key=AIzaSyCkgVHi1bRyvj5Z7ghkAktChEL36JtidLQ&language=ru"));
				
				$latLng = $coor->results[0]->geometry->location->lat.','.$coor->results[0]->geometry->location->lng;
				
				$query = "INSERT INTO `coor`(`address`, `lat`, `lng`) VALUES ('".mysql_escape_string($temp[0].','.$temp[1])."','".$coor->results[0]->geometry->location->lat."','".$coor->results[0]->geometry->location->lng."')";
				$db->setQuery($query);
				$result = $db->execute();
			}
			if( trim($latLng) == ','){ continue; }
		?>
		
		var latLng = new google.maps.LatLng( <?php echo $latLng; ?> );
		var marker = new google.maps.Marker({
			position: latLng,
			icon: "<?php echo $marker; ?>",
			title:  "<?php echo $temp[0]; ?>",
			id: "<?php echo $temp[0].', '.$temp[1]; ?>"
		});
		markers.push(marker);
		
		
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infoWindow.setContent( '<div class="marker_info"><?php echo $temp[0].', '.$temp[1] ; ?></div>' );
				infoWindow.open(map, marker);
				map.setCenter(marker.position);
				map.setZoom(17);
				
				var this_class = marker.id;
				var this_sity = this_class.split(', ');
				var show_count = 0;
				jQuery('.adresses li').each(function(){
					if( jQuery(this).attr('street').toLowerCase().indexOf(', '+this_sity[2].toLowerCase())==-1 ){
						jQuery(this).hide();
					}else{
						jQuery(this).show();
						show_count++;
					}
				});
				jQuery('.groceries_number').html(show_count);
				
				
				jQuery('.adresses li').removeClass('active');
				
				jQuery('.adresses li[street="'+this_class+'"]').addClass('active');
				
				jQuery('.search_city select option[value="'+this_sity[2]+'"]').attr('selected',true);
				
				//jQuery('#k2Container .adresses ul').animate({scrollTop: jQuery('.adresses li.active').offset().top-10}, 1000);
			}
		})(marker, i));
		
		<?php } ?>
		
		
		jQuery('.adresses ul li').each(function(){
			jQuery(this).click(function(){
				
				jQuery('.adresses li').removeClass('active');
				var this_val = jQuery(this).attr('street');
				var this_item = jQuery(this);
				markers.forEach(function(i){
					var marker_val = i.id;
					if( marker_val==this_val ){
						if(i.position.lat()!=''){
							map.setCenter(i.position);
							map.setZoom(17);
							
							this_item.addClass('active');
							infoWindow = new google.maps.InfoWindow()
							infoWindow.setContent('<div class="marker_info">'+marker_val+'</div>');
							infoWindow.open(map, i);
						}
					}
				});
				jQuery('html, body').animate({scrollTop: jQuery('#new_product_map').offset().top-10}, 1000);
			});
		});
		
		
		jQuery('.search_city').change(function(){
			var search_val = jQuery(this).find('select option:selected').val().toLowerCase();
			var show_count = 0;
			jQuery('.adresses li').each(function(){
				if( jQuery(this).attr('street').toLowerCase().indexOf(', '+search_val)==-1 && search_val.length!=0 ){
					jQuery(this).hide();
				}else{
					jQuery(this).show();
					show_count++;
				}
			});
			
			var newLat = 0;
			var newLng = 0;	
			markers.forEach(function(i){
				if( i.id.toLowerCase().indexOf( ', '+search_val ) !=-1 ){
					newLat = newLat > i.position.lat() ? newLat : i.position.lat();
					newLng = newLat < i.position.lng() ? newLng : i.position.lng();
				}
			});
			
			//console.log(newLat+'   '+newLng)
			map.setCenter({
				lat : newLat,
				lng : newLng
			});
			map.setZoom(10);
			jQuery('.groceries_number').html(show_count);
		});
		
		/*-------*/
		var search_val = jQuery('.search_city').find('select option:selected').val().toLowerCase();
		var show_count = 0;
		jQuery('.adresses li').each(function(){
			if( jQuery(this).attr('street').toLowerCase().indexOf(', '+search_val)==-1 && search_val.length!=0 ){
				jQuery(this).hide();
			}else{
				jQuery(this).show();
				show_count++;
			}
		});
		/*
		var newLat = 0;
		var newLng = 0;	
		markers.forEach(function(i){
			if( i.id.toLowerCase().indexOf( ', '+search_val ) !=-1 ){
				newLat = newLat > i.position.lat() ? newLat : i.position.lat();
				newLng = newLat < i.position.lng() ? newLng : i.position.lng();
			}
		});
		map.setCenter({
			lat : newLat,
			lng : newLng
		});
		map.setZoom(6);
		*/
		jQuery('.groceries_number').html(show_count);
		/*-------*/
		
		
		var markerCluster = new MarkerClusterer(map, markers, { 
			imagePath: '<?php echo $marker_claster; ?>' 
		});
      }
    
    </script>
    <script async defer  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkgVHi1bRyvj5Z7ghkAktChEL36JtidLQ&callback=initMap"></script>
	<script src="/blog/templates/gk_simplicity/js/markerclusterer.js"></script>