<?php

/**
 * @package		K2
 * @author		GavickPro http://gavick.com
 */

// no direct access
defined('_JEXEC') or die;

// Template override
jimport('joomla.filesystem.file');
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
$templateParams = JFactory::getApplication()->getTemplate(true)->params;
$override = JPATH_SITE . DS . 'templates' . DS . 'gk_overrides' . DS . $templateParams->get('custom_override', '-1') . DS . 'html' . DS;
$override .=  'com_k2' . DS . 'templates' . DS . 'default' . DS . 'latest.php';

if(
	$templateParams->get('custom_override', '-1') !== '-1' && 
	JFile::exists($override) &&
	__FILE__ !== $override
) :
	include_once($override);
else :
?>

<div id="k2Container" class="blog-page latest-page <?php if($this->params->get('pageclass_sfx')) echo ' '.$this->params->get('pageclass_sfx'); ?>">
	<?php 
	 	$document = JFactory::getDocument();
	 	$renderer = $document->loadRenderer('modules');
	 	
	 	if($document->countModules('breadcrumb')) {
			echo '<div id="gkBreadcrumb">';
			echo '<div class="gkPage">';
			echo $renderer->render('breadcrumb', array('style' => 'none'), null); 
			echo '</div>';
			echo '</div>';
		}
	?>
	
	<div class="gkPage">
		<div id="gk-content-wrapper">
			<div class="item-list">
				<?php foreach($this->blocks as $key=>$block): ?>
				<div class="item-list-block">	
					<h2><?php echo $block->name; ?></h2>
					
					<div class="item-list-items">
						<?php foreach ($block->items as $item) : ?>
							<?php K2HelperUtilities::setDefaultImage($item, 'latest', $this->params); ?>
							<article class="item-view">				    
								<?php if(!empty($item->image)): ?>
							    <a href="<?php echo $item->link; ?>" title="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" class="item-img">
							    	<img src="<?php echo $item->image; ?>" alt="<?php if(!empty($item->image_caption)) echo K2HelperUtilities::cleanHtml($item->image_caption); else echo K2HelperUtilities::cleanHtml($item->title); ?>" />
							    </a>
								<?php endif; ?>
								
								<?php if($item->params->get('latestItemTitle')): ?>
							  	<h3 class="item-title">
							          <?php if ($item->params->get('latestItemTitleLinked')): ?>
							          <a href="<?php echo $item->link; ?>" class="inverse"><?php echo $item->title; ?></a>
							          <?php else: ?>
							          <?php echo $item->title; ?>
							          <?php endif; ?>
							  	</h3>
								<?php endif; ?>
								    
						    	<?php if($item->params->get('latestItemDateCreated',1)): ?>
						    	<!-- Date created -->
						    	<span class="latest-item-date-created">
						    		<time datetime="<?php echo JHtml::_('date', $item->created, JText::_(DATE_W3C)); ?>"> <?php echo JHTML::_('date', $item->created, JText::_('d F Y')); ?> </time>
						    	</span>
						    	<?php endif; ?>
							</article>				
						<?php endforeach; ?>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
		
		<?php 
		 	/*
			$document = JFactory::getDocument();
		 	$renderer = $document->loadRenderer('modules');
		 	
		 	if($document->countModules('sidebar')) {
		 		echo '<aside id="gkSidebar">';
		 		echo '<div>';
		 		echo $renderer->render('sidebar', array('style' => 'gk_style'), null); 
		 		echo '</div>';
		 		echo '</aside>';
		 	}
			*/
		?>
	</div>
</div>
<?php endif; ?>