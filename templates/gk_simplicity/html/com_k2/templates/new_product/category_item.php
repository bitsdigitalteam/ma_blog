<?php

/**
 * @package		K2
 * @author		GavickPro http://gavick.com
 */

// no direct access
defined('_JEXEC') or die;

// Template override
jimport('joomla.filesystem.file');
if(!defined('DS')) define('DS',DIRECTORY_SEPARATOR);
$templateParams = JFactory::getApplication()->getTemplate(true)->params;
$override = JPATH_SITE . DS . 'templates' . DS . 'gk_overrides' . DS . $templateParams->get('custom_override', '-1') . DS . 'html' . DS;
$override .=  'com_k2' . DS . 'templates' . DS . 'default' . DS . 'category_item.php';

if(
	$templateParams->get('custom_override', '-1') !== '-1' && 
	JFile::exists($override) &&
	__FILE__ !== $override
) :
	include_once($override);
else :
?>

<?php

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

?>

<style type="text/css">
	.gkPage{
		padding:0!important;
	}
</style>

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('#gkMainbodyBottom').remove();
	});
</script>

<article class="item-view blog-view"> 	
	<?php echo $this->item->event->BeforeDisplay; ?> 
	<?php echo $this->item->event->K2BeforeDisplay; ?>
	
	<?php 
		$this->item->extra_fields = json_decode($this->item->extra_fields);
	?>
	
	<div class="new_product <?php echo $this->item->extra_fields[19]->value;?> before_readmore">
			<div>
				<div class="new_product_image">
					<a href="<?php echo $this->item->link; ?>">
						<div class="product_image">
							<img src="/blog/<?php echo $this->item->extra_fields[0]->value;?>">
						</div>
					</a>
					
					<a href="<?php echo $this->item->link; ?>">
						<div class="product_readmore">
							<?php echo JText::_(K2_BUY); ?>
						</div>
					</a>
					
				</div>
			
				<div class="new_product_text">
					<a href="<?php echo $this->item->link; ?>">
						<div class="new_product_title">
							<h1><?php echo $this->item->title; ?></h1>
						</div>
					</a>
					
					<div class="new_product_desc">
						
						<?php echo $this->item->introtext; ?>
						
						<div class="icons_row">
							<?php if($this->item->extra_fields[1]->value!='' && $this->item->extra_fields[2]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[1]->value;?>">
								<p class="ench"><?php echo $this->item->extra_fields[2]->value ?></p>
							</div>
							<?php 
								}
								if($this->item->extra_fields[3]->value!='' && $this->item->extra_fields[4]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[3]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[4]->value ?></p>
							</div>
							<?php }?>
						</div>
						<div class="icons_row">
							<?php if($this->item->extra_fields[5]->value!='' && $this->item->extra_fields[6]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[5]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[6]->value ?></p>
							</div>
							<?php 
								}
								if($this->item->extra_fields[7]->value!='' && $this->item->extra_fields[8]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[7]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[8]->value ?></p>
							</div>
								<?php }?>
						</div>
						
						<div class="icons_row">
							<?php if($this->item->extra_fields[9]->value!='' && $this->item->extra_fields[10]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[9]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[10]->value ?></p>
							</div>
							<?php 
								}
								if($this->item->extra_fields[11]->value!='' && $this->item->extra_fields[12]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[11]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[12]->value ?></p>
							</div>
							<?php }?>
						</div>
						<div class="icons_row">
							<?php if($this->item->extra_fields[13]->value!='' && $this->item->extra_fields[14]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[13]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[14]->value ?></p>
							</div>
							<?php 
								}
								if($this->item->extra_fields[15]->value!='' && $this->item->extra_fields[16]->value!='' ) {?>
							<div class="product_ench">
								<img src="/blog/<?php echo $this->item->extra_fields[15]->value ?>">
								<p class="ench"><?php echo $this->item->extra_fields[16]->value ?></p>
							</div>
								<?php }?>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		
	
	
	
	<!--
	<div>
		<header>
		    <?php if($this->item->params->get('catItemDateCreated')): ?>
		    <span class="cat-item-date">
		    	<time datetime="<?php echo JHtml::_('date', $this->item->created, JText::_(DATE_W3C)); ?>"> <?php echo JHTML::_('date', $this->item->created, JText::_('d F Y')); ?> </time>
		    </span>
		    <?php endif; ?> 

		    <?php if($this->item->params->get('catItemTitle')): ?>
	    	<h2 class="item-title">
	            <?php if ($this->item->params->get('catItemTitleLinked')): ?>
	            <a href="<?php echo $this->item->link; ?>" class="inverse"><?php echo $this->item->title; ?></a>
	            <?php else: ?>
	            <?php echo $this->item->title; ?>
	            <?php endif; ?>
	            
	            <?php if($this->item->featured): ?>
	            <sup><i class="gkicon-star"></i></sup>
	            <?php endif; ?>
	    	</h2>
	    	
	    	<?php echo $this->item->event->AfterDisplayTitle; ?> 
	    	<?php echo $this->item->event->K2AfterDisplayTitle; ?>
		    <?php endif; ?>
		    
		    <?php if($this->item->params->get('catItemAuthor')): ?>
		    <span class="cat-item-author">
		        <?php echo JText::_('TPL_GK_LANG_POSTED_BY'); ?>
		        
		        <?php if(isset($this->item->author->link) && $this->item->author->link): ?>
		        <a rel="author" href="<?php echo $this->item->author->link; ?>" title="<?php echo $this->item->author->name; ?>" class="inverse">
		        	<?php echo $this->item->author->name; ?>
		        </a>
		        <?php else: ?>
		        	<?php echo $this->item->author->name; ?>
		        <?php endif; ?>
		    </span>
		    <?php endif; ?>  
	    </header>

		<?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
	    <div>
		    <a href="<?php echo $this->item->link; ?>" title="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" class="cat-item-image">
		    	<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" />
		    </a>
	    </div>
		<?php endif; ?>
		
		<?php if($this->item->params->get('catItemIntroText')): ?>
		<div class="cat-item-intro-text">
			<?php echo $this->item->introtext; ?>

			<?php if ($this->item->params->get('catItemReadMore')): ?>
			<a class="cat-readon" href="<?php echo $this->item->link; ?>">
				<?php echo JText::_('TPL_GK_LANG_K2_MORE'); ?>
			</a>
			<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>

	-->
    
	<?php echo $this->item->event->AfterDisplay; ?> 
    <?php echo $this->item->event->K2AfterDisplay; ?>
	
</article>
<?php endif; ?>