<?php

// Pagination override

// no direct access
defined('_JEXEC') or die('Restricted access');

function pagination_list_render($list) {
	// Reverse output rendering for right-to-left display.
	$html = '<nav class="pagination"><ul>';
	
	$html .= '<div class="nav_rows">';
	$html .= '<li class="pagination-prev">'.$list['previous']['data'].'</li>';
	$html .= '<li class="pagination-next">'. $list['next']['data'].'</li>';
	$html .= '</div>';
	
	$html .= '<div class="numeric_row">';
	for($i = 1; $i <= count($list['pages']); $i++) {
		if( $i % 20 ==1 || $i ==1){
			$html .= '<div class="num_row">';
		}
		$html .= '<li>'.$list['pages'][$i]['data'].'</li>';
		if( $i % 20 ==0 ){
			$html .= '</div>';
		}
	}
	$html .= '</div>';
	
	$html .= '</ul></nav>';

	return $html;
}
	
function pagination_item_active($item) {
	$app = JFactory::getApplication();
	
	if ($app->isAdmin()) {
		if ($item->base > 0) {
			return "<a title=\"".$item->text."\" onclick=\"document.adminForm." . $this->prefix . "limitstart.value=".$item->base."; Joomla.submitform();return false;\">".$item->text."</a>";
		} else {
			return "<a title=\"".$item->text."\" onclick=\"document.adminForm." . $this->prefix . "limitstart.value=0; Joomla.submitform();return false;\">".$item->text."</a>";
		}
	} else {
		return "<a title=\"".$item->text."\" href=\"".$item->link."\" class=\"pagenav\">".$item->text."</a>";
	}
}

function pagination_item_inactive($item) {
	$app = JFactory::getApplication();
	
	if ($app->isAdmin()) {
		return "<span>".$item->text."</span>";
	} else {
		return "<span class=\"pagenav\">".$item->text."</span>";
	}
}

// EOF