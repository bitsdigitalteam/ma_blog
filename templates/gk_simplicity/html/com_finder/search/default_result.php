<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_finder
 *
 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('_JEXEC') or die;

require_once JPATH_SITE . DS . 'components' . DS . 'com_k2' . DS . 'helpers' . DS . 'route.php';

// Get the mime type class.
$mime = !empty($this->result->mime) ? 'mime-' . $this->result->mime : null;

// Get the base url.
$base = JURI::getInstance()->toString(array('scheme', 'host', 'port'));

// Get the route with highlighting information.
if (!empty($this->query->highlight) && empty($this->result->mime) && $this->params->get('highlight_terms', 1) && JPluginHelper::isEnabled('system', 'highlight')) {
	$route = $this->result->route . '&highlight=' . base64_encode(serialize($this->query->highlight));
} else {
	$route = $this->result->route;
}

$cat_alias = $this->result->language == 'uk-UA' ? 'stattya' : 'statya';

$link = urldecode(JRoute::_(K2HelperRoute::getItemRoute(urlencode($this->result->alias), $this->result->catid.':'.urlencode($cat_alias))));

?>

<dt class="result-title <?php echo $mime; ?>">
	<a href="<?php 
		
		$link = str_replace('component/k2/','',$link); 
		
		$link = str_replace('/ru/item/','/ua/stattya/',$link);
		
		$link = str_replace('ua/item/','ru/statya/',$link);
		
		$link = str_replace('/item/','/',$link);
		
		echo $link;
	?>"><?php echo $this->result->title; ?></a>
</dt>
<dd class="result-text<?php echo $this->pageclass_sfx; ?>">
	<?php echo JHtml::_('string.truncate', $this->result->description, $this->params->get('description_length', 255)); ?>
</dd>

<?php if ($this->params->get('show_url', 1)): ?>
<dd class="result-url<?php echo $this->pageclass_sfx; ?>">
	<?php echo $base . JRoute::_($this->result->route); ?>
</dd>
<?php endif;
