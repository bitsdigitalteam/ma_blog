<?php

/**
 *
 * Default view
 *
 * @version             1.0.0
 * @package             Gavern Framework
 * @copyright			Copyright (C) 2010 - 2011 GavickPro. All rights reserved.
 *               
 */
 
// No direct access.
defined('_JEXEC') or die;
//
$app = JFactory::getApplication();
$user = JFactory::getUser();
// getting User ID
$userID = $user->get('id');
// getting params
$option = JRequest::getCmd('option', '');
$view = JRequest::getCmd('view', '');
// defines if com_users
define('GK_COM_USERS', $option == 'com_users' && ($view == 'login' || $view == 'registration'));
// other variables
$btn_login_text = ($userID == 0) ? JText::_('TPL_GK_LANG_LOGIN') : JText::_('TPL_GK_LANG_LOGOUT');
$tpl_page_suffix = $this->page_suffix != '' ? ' class="'.$this->page_suffix.'"' : '';
// make sure that the modal will be loaded
JHTML::_('behavior.modal');


$document = JFactory::getDocument();
$document->setMetaData('robots', "index,follow");


?>
<!DOCTYPE html>
<html lang="<?php echo $this->APITPL->language; ?>" <?php echo $tpl_page_suffix; ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
    <?php if($this->API->get('rwd', 1)) : ?>
    	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">
	<?php else : ?>
		<meta name="viewport" content="width=<?php echo $this->API->get('template_width', 1020)+80; ?>">
	<?php endif; ?>
	
	<meta property="fb:app_id" content="872591456177194" />
	
    <jdoc:include type="head" />
    <?php $this->layout->loadBlock('head'); ?>
	<?php $this->layout->loadBlock('cookielaw'); ?>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-33607450-1', 'auto');
	  ga('send', 'pageview');

</script>

	
	
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '278051232359344');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=278051232359344&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '370478016638379');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=370478016638379&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1250596214977442'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=1250596214977442&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

	
</head>
<body class="imageBg" <?php /*echo $tpl_page_suffix;*/ ?><?php if($this->browser->get("tablet") == true) echo ' data-tablet="true"'; ?><?php if($this->browser->get("mobile") == true) echo ' data-mobile="true"'; ?><?php $this->layout->generateLayoutWidths(); ?> data-layout="<?php echo $this->API->get('layout_position', 'left'); ?>">	
	<?php if ($this->browser->get('browser') == 'ie7' || $this->browser->get('browser') == 'ie6') : ?>
	<!--[if lte IE 7]>
	<div id="ieToolbar"><div><?php echo JText::_('TPL_GK_LANG_IE_TOOLBAR'); ?></div></div>
	<![endif]-->
	<?php endif; ?>
	
    <header id="gkHeader">
    	<div>
	    	<div class="gkPage" id="gkHeaderNav">
				
			    <?php 
					$lang = JFactory::getLanguage();
					$lang = explode('-',$lang->getTag());
					echo '<div class="gradient"></div>
						  <a href="https://milkalliance.com.ua/'.$lang[0].'/" id="gkLogo">
							<img src="https://milkalliance.com.ua/design/milka/img/svg/logo_'.$lang[0].'.svg" alt="Группа компаний «Молочний Альянс»">
						  </a>';
					//$this->layout->loadBlock('logo'); 
					
				?>
			    <jdoc:include type="modules" name="header_search"/>
			    <?php if($this->API->get('show_menu', 1)) : ?>
			    <div id="gkMainMenu">
			    	<?php
						$this->mainmenu->loadMenu($this->API->get('menu_name','mainmenu').$lang[0]); 
			    	    $this->mainmenu->genMenu($this->API->get('startlevel', 0), $this->API->get('endlevel',-1));
			    	?>
			    </div>
				
			    <?php endif; ?>
			    
			    <?php if($this->API->get('show_menu', 1)) : ?>
		    	<div id="gkMobileMenu">
		    		<?php echo JText::_('TPL_GK_LANG_MOBILE_MENU'); ?>
		    		<select onChange="window.location.href=this.value;">
		    		<?php 
		    		    $this->mobilemenu->loadMenu($this->API->get('menu_name','mainmenu').$lang[0]); 
		    		    $this->mobilemenu->genMenu($this->API->get('startlevel', 0), $this->API->get('endlevel',-1));
		    		?>
		    		</select>
		    	</div>
		    	<?php endif; ?>
	    	</div>
    	</div>
    	
   		<?php if($this->API->modules('header')) : ?>
   		<div id="gkHeaderMod" class="gkPage">
   			<jdoc:include type="modules" name="header" style="<?php echo $this->module_styles['header']; ?>" />
   		</div>
   		<?php endif; ?>
    </header>

	<?php if(count($app->getMessageQueue())) : ?>
	<jdoc:include type="message" />
	<?php endif; ?>
	
	<div id="gkPageContent" class="gkPage <?php echo JRequest::getVar('view')=='item' ? 'custom-p' : 'custom-t' ?>">
    	<section id="gkContent">					
			<div id="gkContentWrap"<?php if($this->API->get('sidebar_position', 'right') == 'left') : ?> class="gkSidebarLeft"<?php endif; ?>>
				<?php if($this->API->modules('top1')) : ?>
				<section id="gkTop1" class="gkCols3<?php if($this->API->modules('top1') > 1) : ?> gkNoMargin<?php endif; ?>">
					<div>
						<jdoc:include type="modules" name="top1" style="<?php echo $this->module_styles['top1']; ?>"  modnum="<?php echo $this->API->modules('top1'); ?>" modcol="3" />
					</div>
				</section>
				<?php endif; ?>
				
				<?php if($this->API->modules('top2')) : ?>
				<section id="gkTop2" class="gkCols3<?php if($this->API->modules('top2') > 1) : ?> gkNoMargin<?php endif; ?>">
					<div>
						<jdoc:include type="modules" name="top2" style="<?php echo $this->module_styles['top2']; ?>" modnum="<?php echo $this->API->modules('top2'); ?>" modcol="3" />
					</div>
				</section>
				<?php endif; ?>
				
				<?php if($this->API->modules('breadcrumb') || $this->getToolsOverride()) : ?>
				<section id="gkBreadcrumb">
					<?php if($this->API->modules('breadcrumb')) : ?>
					<jdoc:include type="modules" name="breadcrumb" style="<?php echo $this->module_styles['breadcrumb']; ?>" />
					<?php endif; ?>
					
					<?php if($this->getToolsOverride()) : ?>
						<?php $this->layout->loadBlock('tools/tools'); ?>
					<?php endif; ?>
				</section>
				<?php endif; ?>
				
				<?php if($this->API->modules('mainbody_top')) : ?>
				<section id="gkMainbodyTop">
					<jdoc:include type="modules" name="mainbody_top" style="<?php echo $this->module_styles['mainbody_top']; ?>" />
				</section>
				<?php endif; ?>	
				
				<section id="gkMainbody">
					<?php if(($this->layout->isFrontpage() && !$this->API->modules('mainbody')) || !$this->layout->isFrontpage()) : ?>
						<jdoc:include type="component" />
					<?php else : ?>
						<jdoc:include type="modules" name="mainbody" style="<?php echo $this->module_styles['mainbody']; ?>" />
					<?php endif; ?>
				</section>
				
				<?php if($this->API->modules('mainbody_bottom')) : ?>
				<section id="gkMainbodyBottom">
					<jdoc:include type="modules" name="mainbody_bottom" style="<?php echo $this->module_styles['mainbody_bottom']; ?>" />
				</section>
				<?php endif; ?>
			</div>
			
			<?php if($this->API->modules('sidebar') && JRequest::getVar('view')=='item'  ) : ?>
			<aside id="gkSidebar"<?php if($this->API->modules('sidebar') == 1) : ?> class="gkOnlyOne"<?php endif; ?>>
				<div>
					<jdoc:include type="modules" name="sidebar" style="<?php echo $this->module_styles['sidebar']; ?>" />
				</div>
			</aside>
			<?php endif;?>
    	</section>
    	
    	<!--[if IE 8]>
    	<div class="ie8clear"></div>
    	<![endif]-->
	</div>
	    
	<?php if($this->API->modules('bottom1') && ( strstr($_SERVER['REQUEST_URI'],'stattya') || strstr($_SERVER['REQUEST_URI'],'statya') ) ) : ?>
	<section id="gkBottom1">
		<div class="gkCols6<?php if($this->API->modules('bottom1') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
			<jdoc:include type="modules" name="bottom1" style="<?php echo $this->module_styles['bottom1']; ?>" modnum="<?php echo $this->API->modules('bottom1'); ?>" />
			
			<!--[if IE 8]>
			<div class="ie8clear"></div>
			<![endif]-->
		</div>
	</section>
	<?php endif; ?>
    
    <?php if($this->API->modules('bottom2')) : ?>
    <section id="gkBottom2">
    	<div class="gkCols6<?php if($this->API->modules('bottom2') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
    		<jdoc:include type="modules" name="bottom2" style="<?php echo $this->module_styles['bottom2']; ?>" modnum="<?php echo $this->API->modules('bottom2'); ?>" />
    		
    		<!--[if IE 8]>
    		<div class="ie8clear"></div>
    		<![endif]-->
    	</div>
    </section>
    <?php endif; ?>
    
    <?php if($this->API->modules('bottom3')) : ?>
    <section id="gkBottom3">
    	<div class="gkCols6<?php if($this->API->modules('bottom3') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
    		<jdoc:include type="modules" name="bottom3" style="<?php echo $this->module_styles['bottom3']; ?>" modnum="<?php echo $this->API->modules('bottom3'); ?>" />
    		
    		<!--[if IE 8]>
    		<div class="ie8clear"></div>
    		<![endif]-->
    	</div>
    </section>
    <?php endif; ?>
    
    <?php if($this->API->modules('bottom4')) : ?>
    <section id="gkBottom4">
    	<div class="gkCols6<?php if($this->API->modules('bottom4') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
    		<jdoc:include type="modules" name="bottom4" style="<?php echo $this->module_styles['bottom4']; ?>" modnum="<?php echo $this->API->modules('bottom4'); ?>" />
    		
    		<!--[if IE 8]>
    		<div class="ie8clear"></div>
    		<![endif]-->
    	</div>
    </section>
    <?php endif; ?>
    
    <?php if($this->API->modules('bottom5')) : ?>
    <section id="gkBottom5">
    	<div class="gkCols6<?php if($this->API->modules('bottom5') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
    		<jdoc:include type="modules" name="bottom5" style="<?php echo $this->module_styles['bottom5']; ?>" modnum="<?php echo $this->API->modules('bottom5'); ?>" />
    		
    		<!--[if IE 8]>
    		<div class="ie8clear"></div>
    		<![endif]-->
    	</div>
    </section>
    <?php endif; ?>
    
    <?php if($this->API->modules('bottom6')) : ?>
    <section id="gkBottom6">
    	<div class="gkCols6<?php if($this->API->modules('bottom6') > 1) : ?> gkNoMargin<?php endif; ?> gkPage">
    		<jdoc:include type="modules" name="bottom6" style="<?php echo $this->module_styles['bottom6']; ?>" modnum="<?php echo $this->API->modules('bottom6'); ?>" />
    		
    		<!--[if IE 8]>
    		<div class="ie8clear"></div>
    		<![endif]-->
    	</div>
    </section>
    <?php endif; ?>
    
    <?php if($this->API->modules('lang')) : ?>
    <section id="gkLang">
    	<div class="gkPage">
         	<jdoc:include type="modules" name="lang" style="<?php echo $this->module_styles['lang']; ?>" />
         </div>
    </section>
    <?php endif; ?>
    
    <?php $this->layout->loadBlock('footer'); ?>
   		
   	<?php $this->layout->loadBlock('social'); ?>
   		
	<jdoc:include type="modules" name="debug" />
</body>
</html>