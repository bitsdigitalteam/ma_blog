/**
 * =============================================================
 * RAXO All-mode K2 J3.0 - Interface JS
 * -------------------------------------------------------------
 * @package		RAXO All-mode K2
 * @copyright	Copyright (C) 2009-2013 RAXO Group
 * @license		RAXO Commercial License
 * 				This file is forbidden for redistribution
 * @link		http://raxo.org
 * =============================================================
 */


(function($){
	$(document).ready(function(){ 

		// Source Selection
		var	source_selection	= $('#jform_params_source_selection'),
			source_cat			= $('#jform_params_source_cat').closest('.control-group'),
			source_itm			= $('#jform_params_source_itm').closest('.control-group');

		// Default Settings
		if (source_selection.find('input:checked').val() == 'cat') {
			source_itm.toggleClass('hide-field');
		} else {
			source_cat.toggleClass('hide-field');
		}

		// Changed Settings
		source_selection.find('label:not(.active)').click(function(){
			source_cat.toggleClass('hide-field');
			source_itm.toggleClass('hide-field');
		});

	})
})(jQuery);