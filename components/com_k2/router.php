<?php
/**
 * @version		$Id: router.php 1618 2012-09-21 11:23:08Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die ;


function K2BuildRoute(&$query)
{	
	
	$site	  = JFactory::getApplication();
	
	if($site->isSite()){
		$items= array();
		$items= getK2ItemsByAlias();	 
	};
	
	
    $segments = array();
    $application = JFactory::getApplication();
    $menu = $application->getMenu();
    if (empty($query['Itemid']))
    {
        $menuItem = $menu->getActive();
    }
    else
    {
        $menuItem = $menu->getItem($query['Itemid']);
    }
    $mView = ( empty($menuItem->query['view'])) ? null : $menuItem->query['view'];
    $mTask = ( empty($menuItem->query['task'])) ? null : $menuItem->query['task'];
    $mId = ( empty($menuItem->query['id'])) ? null : $menuItem->query['id'];
    $mTag = ( empty($menuItem->query['tag'])) ? null : $menuItem->query['tag'];

    if (isset($query['layout']))
    {
        unset($query['layout']);
    }

    if ($mView == @$query['view'] && $mTask == @$query['task'] && $mId == @intval($query['id']) && @intval($query['id']) > 0)
    {
        unset($query['view']);
        unset($query['task']);
        unset($query['id']);
    }

    if ($mView == @$query['view'] && $mTask == @$query['task'] && $mTag == @$query['tag'] && isset($query['tag']))
    {
        unset($query['view']);
        unset($query['task']);
        unset($query['tag']);
    }

    if (isset($query['view']))
    {
        $view = $query['view'];
        $segments[] = $view;
        unset($query['view']);
    }

    if (@ isset($query['task']))
    {
        $task = $query['task'];
        $segments[] = $task;
        unset($query['task']);
    }

    if (isset($query['id']))
    {
        $id = $query['id'];
        $segments[] = $id;
        unset($query['id']);
    }

    if (isset($query['cid']))
    {
        $cid = $query['cid'];
        $segments[] = $cid;
        unset($query['cid']);
    }

    if (isset($query['tag']))
    {
        $tag = $query['tag'];
        $segments[] = $tag;
        unset($query['tag']);
    }

    if (isset($query['year']))
    {
        $year = $query['year'];
        $segments[] = $year;
        unset($query['year']);
    }

    if (isset($query['month']))
    {
        $month = $query['month'];
        $segments[] = $month;
        unset($query['month']);
    }

    if (isset($query['day']))
    {
        $day = $query['day'];
        $segments[] = $day;
        unset($query['day']);
    }

    if (isset($query['task']))
    {
        $task = $query['task'];
        $segments[] = $task;
        unset($query['task']);
    }

    
    
    
    if($site->isSite()){
    
    if(isset($segments[0])){
    
    $keys   		 = array();	
    	
    
    if ($segments[0] == 'itemlist')
    {
		switch($segments[1])
        {

            case 'category' :
                if (isset($segments[2]) && is_array($items) && isset($items['categories']) && is_array($items['categories'])){
                	
					$catid	 	= '';
                	       	
                	if(JString::strpos($segments[2],':') !== false){
                		list($catid,$trash) = explode(':',$segments[2],2);
                		$catid				= (int)$catid;
                	} else {
                		$catid				= intval($segments[2]);
                	}
                	
                	if(!empty($catid) && isset($items['categories'][$catid]) && !empty($items['categories'][$catid]) && !in_array($items['categories'][$catid],$items['items']) && !in_array($items['categories'][$catid],array('itemlist','item','tag','comments','reportSpammer'))){
                	
                		$segments	= array();
                		$keys		= array_keys($items['categories'],$items['categories'][$catid]);
	                	
                		
                		if(sizeof($keys) > 1){
	                		$segments[] = $catid.':'.$items['categories'][$catid];
	                	} else {
	                		$segments[] = $items['categories'][$catid];
	                	}

                	} 
                	
                	                	
                }
                break;

            case 'tag' :
                 
            	if(isset($items['tags']) && is_array($items['tags']) && sizeof($items['tags']) && !empty($segments[2])){
            		
            		$languageFilter 	= JFactory::getLanguage();
					$tag				= $languageFilter->transliterate($segments[2]);
            		$tag 				= JApplication::stringURLSafe($tag);
					
            		if(in_array($tag,$items['tags'])){
            			$segments			= array();	
            			$segments[0]		= 'tag';
            			$segments[1]		= $tag;
            		}
            	};
            	
                break;

           }

    }
    
	else if ($segments[0] == 'item')
    {
    	
    		 if($segments[1] != 'edit' && $segments[1] != 'download'){
            		
    	     if (isset($segments[1]) && is_array($items) && isset($items['items']) && is_array($items['items'])){
             	
					$itemid	 	= '';
                	       	
                	if(JString::strpos($segments[1],':') !== false){
                		list($itemid,$trash) = explode(':',$segments[1],2);
                		$itemid				= (int)$itemid;
                	} else {
                		$itemid				= intval($segments[1]);
                	}
                	
                	if(!empty($itemid) && isset($items['items'][$itemid]) && !empty($items['items'][$itemid]) && !in_array($items['items'][$itemid],array('itemlist','item','tag','comments','reportSpammer'))){
                	
                		$segments	= array();
                		$keys		= array_keys($items['items'],$items['items'][$itemid]);
	                	
                		if(sizeof($keys) > 1){
	                		$segments[] = $itemid.':'.$items['items'][$itemid];
	                	} else {
	                		$segments[] = $items['items'][$itemid];
	                	}

                	} 
                	
                	                	
             }
    	
	         };
    }

    }
    }
    
    return $segments;
}

function getK2ItemsByAlias(){
	static $items;
	static $searched;
	
	if(!isset($searched) || empty($searched)){
		
		$items				= array('items'=>array(),'categories'=>array(),'tags'=>array(),'tags_xref'=>array());
		
		$user 				= JFactory::getUser();
	    $aid 				= $user->get('aid');
	    $db 				= JFactory::getDBO();
	     
	        
	    $jnow 				= JFactory::getDate();
	    $now 				= $jnow->toSql();
	    $nullDate 			= $db->getNullDate();
		$mainframe 			= JFactory::getApplication();
		$languageFilter 	= $mainframe->getLanguageFilter();
		$task 				= JRequest::getCmd('task');
		
		JLoader::register('K2HelperUtilities', JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'utilities.php');
		
		$params 			= K2HelperUtilities::getParams('com_k2');
		
		
		$query				= "";
		$query				= "SELECT i.alias,i.id FROM #__k2_items AS i 
							WHERE i.published = 1 
							AND i.access IN(".implode(',', $user->getAuthorisedViewLevels()).")"
							." AND i.trash = 0";
					
					
		if ($languageFilter)
        {
                $languageTag = JFactory::getLanguage()->getTag();
                $query 		.= " AND i.language IN (".$db->quote($languageTag).",".$db->quote('*').")";
        }
        
        if (!($task == 'user' && !$user->guest && $user->id == JRequest::getInt('id')))
        {
            $query 			.= " AND ( i.publish_up = ".$db->Quote($nullDate)." OR i.publish_up <= ".$db->Quote($now)." )";
            $query 			.= " AND ( i.publish_down = ".$db->Quote($nullDate)." OR i.publish_down >= ".$db->Quote($now)." )";
        }
        			
					
		$db->setQuery($query);
		$items['items'] 	= $db->loadAssocList('id','alias');
					
		$query				= "";
		$query				= "SELECT c.alias,c.id FROM #__k2_categories AS c "			
							." WHERE c.published = 1"
								." AND c.access IN(".implode(',', $user->getAuthorisedViewLevels()).")"." AND c.trash = 0";
		
		if ($languageFilter)
        {
                $languageTag = JFactory::getLanguage()->getTag();
                $query 		.= " AND c.language IN (".$db->quote($languageTag).",".$db->quote('*').") ";
        }
        
        
        $db->setQuery($query);
		$items['categories'] = $db->loadAssocList('id','alias');
		
            
        $query				= "";
        $query 				= "SELECT id, name FROM #__k2_tags AS t WHERE t.published = 1";

        
        $db->setQuery($query);
		$items['tags'] 		= $db->loadAssocList('id','name');
		$items['tags_xref'] = $items['tags'];
		
		$languageFilter 	= JFactory::getLanguage();
		
		foreach ($items['tags'] as $id=>$tag) {
			$items['tags'][$id] = $languageFilter->transliterate($tag);
			$items['tags'][$id] = JApplication::stringURLSafe($items['tags'][$id]);
		}
		
		       
		$searched			= true;	
		
		 
		
    }

    
    
	return $items;
}

function K2ParseRoute($segments)
{
	
	$site	  							= JFactory::getApplication();
	
	if($site->isSite()){
		$items							= array();
		$items	  						= getK2ItemsByAlias();
	
		$vars							= array();
	    
	    if(!in_array($segments[0],array('itemlist','item','tag','comments','reportSpammer'))){
			 
			 $new_segments 				= array();
			 $sizeof					= sizeof($segments);
			 
			 for($i = $sizeof - 1; $i > -1; $i--){
			 
			 	$array					= array();
			 	
			 	if(JString::strpos($segments[$i],':') !== false){
			 		$array				= explode(':',$segments[$i],2);
			 	} else {
			 		$array				= array($segments[$i]);
			 	} 
			 	
			 	$id 		  			= 0;
			 	$found					= false;	
			 	$array[0] 				= JString::trim($array[0]);
			  	$id						= $array[0];
			 	$matches				= array(); 
			 	
			 	if($id && preg_match('~^[0-9]+$~',$id,$matches) && isset($array[1]) && !empty($array[1])){
			 		
			 		$segments[$i] 		= $array[1];
			 		
			 		if(isset($items['items'][$id]) && $items['items'][$id] == $segments[$i]){
			 			
			 			$new_segments[] = 'item';
			 			$new_segments[] = $id;
			 			$found	  		= true;
			 		
			 		} else if(isset($items['categories'][$id]) && $items['categories'][$id] == $segments[$i]){
			 			
			 			$new_segments[] = 'itemlist';
			 			$new_segments[] = 'category';
			 			$new_segments[] = $id;
			 			$found	  		= true;
			 		}
			 		
			 		 
			 		
			 	}
			 	 
			 	
			 	if(!$found){
			 		
			 		$segments[$i] 		= JString::str_ireplace(':','-',$segments[$i]);
			 		$key		  		= 0;
			 		
			 		if(($key = array_search($segments[$i],$items['items'])) !== false){
			 			
			 			$new_segments[] = 'item';
			 			$new_segments[] = $key;
			 			$found	  		= true;
			 		
			 		} else if(($key = array_search($segments[$i],$items['categories'])) !== false){
			 			
			 			$new_segments[] = 'itemlist';
			 			$new_segments[] = 'category';
			 			$new_segments[] = $key;
			 			$found	  		= true;
			 		}
					 		
			 	}
			 	
			 	if($found){
			 							break;
			 	}  
			 }
			 
			 if(sizeof($new_segments) && $found){
			 	$segments				= array();
			 	$segments				= $new_segments;
			 }
			  
		} else if(isset($segments[0]) && $segments[0]  == 'tag' && isset($segments[1]) && !empty($segments[1])){
			 $segments[0] 				= 'itemlist';
			 $segments[2] 				= $segments[1];
			 $segments[1] 				= 'tag';
		};
	};
	
	
	$vars 	= array();
    $vars['view'] = $segments[0];
    if (!isset($segments[1]))
        $segments[1] = '';
    $vars['task'] = $segments[1];

    if ($segments[0] == 'itemlist')
    {

        switch($segments[1])
        {

            case 'category' :
                if (isset($segments[2]))
                    $vars['id'] = $segments[2];
                break;

            case 'tag' :

            	$key 				 = 0;
            	
            	
				if($site->isSite()){
	            	if (isset($segments[2]) && !empty($segments[2])){
	            		if(JString::strpos($segments[2],':') !== false){
		            		$segments[2]	 = JString::str_ireplace(':','-',$segments[2]);
		            	}	
		            			
		            	if(($key 			 = array_search($segments[2],$items['tags'])) !== false){
		            		if(isset($items['tags_xref'][$key])){
		            			$segments[2] = $items['tags_xref'][$key];
		            		};
		            	}
	            	}
				};
					
            	if (isset($segments[2]))
                    $vars['tag'] = $segments[2];
                break;

            case 'user' :
                if (isset($segments[2]))
                    $vars['id'] = $segments[2];
                break;

            case 'date' :
                if (isset($segments[2]))
                    $vars['year'] = $segments[2];
                if (isset($segments[3]))
                    $vars['month'] = $segments[3];
                if (isset($segments[4]))
                {
                    $vars['day'] = $segments[4];
                }
                break;
        }

    }
    
	else if ($segments[0] == 'item')
    {

        switch($segments[1])
        {

            case 'edit' :
                if (isset($segments[2]))
                    $vars['cid'] = $segments[2];
                break;

            case 'download' :
              	if (isset($segments[2]))
                    $vars['id'] = $segments[2];
                break;

            default :
                $vars['id'] = $segments[1];
                break;
        }

    }

    if ($segments[0] == 'comments' && isset($segments[1]) && $segments[1] == 'reportSpammer')
    {
        $vars['id'] = $segments[2];
    }

    return $vars;
}
